#!/bin/bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cloudformationFile="${currentDir}/distribution.json"
origin=$1
vanity=$2

if [ -z "${origin}" ]; then
    echo "first argument should be the origin url"
    exit 1
fi

if [ -z "${vanity}" ]; then
    echo "second argument should be the base vanity url"
    exit 1
fi

IFS='/ ' read -r -a originArray <<< "$origin"

if [[ "$originArray[0]" == *"http"* ]]
then
    originPath="${originArray[3]}"
else
    originPath="${originArray[1]}"
fi

echo ${originPath}
region=us-east-1
stackName=${vanity//./"-"}
echo ${stackName}
command="create-stack"
aws cloudformation describe-stacks --stack-name ${stackName} --output text --region ${region} &> /dev/null
if [ $? -eq 0 ]; then
    command="update-stack"
fi

aws cloudformation ${command} \
    --stack-name ${stackName} \
    --parameters \
        ParameterKey=OriginPath,ParameterValue=${originPath} \
        ParameterKey=AliasUrl,ParameterValue=${vanity} \
    --template-body "file://${cloudformationFile}" \
    --region ${region} \
    --output table
