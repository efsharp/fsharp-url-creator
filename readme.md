To create new Cloudfront distribution and Route53 entry, run:

```
#!bash

./add-distribution.sh ORIGIN_CUSTOM_CAMPAIGN_URL BASE_VANITY_URL
```

Example:

```
#!bash

./add-distribution.sh https://campaigns.efsharp.com/f3.ng.sc.twitter.nd/index.html spotify-twittermusicuk.com

```


This will create the Cloudfront distribution and the following Route53 RecordSets:

example.com

www.example.com

You will still need to take the Name Servers from Route53 and enter them in GoDaddy