#!/bin/bash
# run '.sh appId vanity url ( .e.g - ./add-distribution-adstudio.sh 10010 nervespotifysweepstakes.com)'
currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cloudformationFile="${currentDir}/distribution-adstudio.json"
adstudioId=$1
vanity=$2

if [ -z "${adstudioId}" ]; then
    echo "first argument should be the origin url"
    exit 1
fi

if [ -z "${vanity}" ]; then
    echo "second argument should be the base vanity url"
    exit 1
fi

echo ${adstudioId}
echo ${vanity}

region=us-east-1
stackName=${vanity//./"-"}
echo ${stackName}
command="create-stack"
aws cloudformation describe-stacks --stack-name ${stackName} --output text --region ${region} &> /dev/null
if [ $? -eq 0 ]; then
    command="update-stack"
fi

aws cloudformation ${command} \
    --stack-name ${stackName} \
    --parameters \
        ParameterKey=OriginDomain,ParameterValue=appserver.efsharp.com \
        ParameterKey=OriginPath,ParameterValue=application \
        ParameterKey=RootObject,ParameterValue=${adstudioId} \
        ParameterKey=AliasUrl,ParameterValue=${vanity} \
    --template-body "file://${cloudformationFile}" \
    --region ${region} \
    --output table
